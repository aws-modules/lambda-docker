resource "aws_iam_role_policy" "this" {
  count  = var.attach_extra_policy ? 1 : 0
  name   = local.global_name
  role   = aws_iam_role.this.id
  policy = var.policy
}

resource "aws_iam_role" "this" {
  name               = local.global_name
  assume_role_policy = var.assume_role_policy
  path               = "/"
  description        = local.global_name
  tags               = local.tags
}

resource "aws_iam_role_policy_attachment" "AWSLambdaBasicExecutionRole_Policy" {
  role       = aws_iam_role.this.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}

resource "aws_lambda_function" "this" {
  function_name = local.global_name
  role          = aws_iam_role.this.arn
  architectures = var.architectures
  description   = local.global_name
  dynamic "environment" {
    for_each = length(keys(var.environment_variables)) == 0 ? {} : tomap({ "environment_variables" = var.environment_variables })
    content {
      variables = environment.value
    }
  }
  source_code_hash = var.source_code_hash
  image_uri        = var.image_uri
  package_type     = var.package_type
  memory_size      = var.memory_size
  timeout          = var.timeout
  publish          = var.publish
  tags             = local.tags
}
